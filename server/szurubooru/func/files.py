from typing import Any, List, Optional

from szurubooru.storage import adapter


def delete(path: str) -> None:
    adapter.delete(path)


def has(path: str) -> bool:
    return adapter.has(path)


def scan(path: str) -> List[Any]:
    return adapter.scan(path)


def move(source_path: str, target_path: str) -> None:
    adapter.move(source_path, target_path)


def get(path: str) -> Optional[bytes]:
    return adapter.get(path)


def save(path: str, content: bytes) -> None:
    adapter.save(path, content)
