#!/usr/bin/dumb-init /bin/sh

# Integrate environment variables
sed -i "s|__BACKEND__|${BACKEND_HOST}|" \
	/etc/nginx/nginx.conf
sed -i "s|__BASEURL__|${BASE_URL:-/}|g" \
	/var/www/index.htm \
	/var/www/manifest.json


if [ -n "$S3_DOMAIN" ]; then
  sed -i "s/{{DATA_INCLUDE}}/include s3.conf;/g" /etc/nginx/nginx.conf;
  sed -i "s/{{S3_DOMAIN}}/${S3_DOMAIN}/g" /etc/nginx/s3.conf;
  if [ -n "$S3_PREFIX" ]; then
    S3_DOMAIN_PREFIX="${S3_DOMAIN}/${S3_PREFIX}"
  else
    S3_DOMAIN_PREFIX="$S3_DOMAIN"
  fi
  sed -i "s/{{S3_DOMAIN_PREFIX}}/${S3_DOMAIN_PREFIX}/g" /etc/nginx/s3.conf
else
  sed -i "s/{{DATA_INCLUDE}}/include local.conf;/g" /etc/nginx/nginx.conf;
fi


# Start server
exec nginx
